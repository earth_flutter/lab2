import "package:flutter/material.dart";

class Counter extends StatefulWidget {
  @override
  _counterState createState() => _counterState();

}

class _counterState extends State<StatefulWidget> {
  int _counter = 0;
  void _increment(){
    setState(() {
      _counter++;
    });
    
  }
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        ElevatedButton(onPressed: _increment, child: Text('Increment')),
        SizedBox(width: 16,),
        Text('counter: $_counter'),
        ],
    );
  }

}

void main(){
  runApp(MaterialApp(
    home: Scaffold(
      body: Center(
        child: Counter(),
        ),
    )
  ));
}